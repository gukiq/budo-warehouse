package org.budo.warehouse.service.util;

import java.util.ArrayList;
import java.util.List;

import org.budo.warehouse.service.entity.EntryBuffer;

/**
 * @author limingwei
 */
public class EntryBufferServiceUtil {
    public static List<Integer> getIds(List<EntryBuffer> entryBuffers) {
        List<Integer> ids = new ArrayList<Integer>();
        for (EntryBuffer entryBuffer : entryBuffers) {
            ids.add(entryBuffer.getId());
        }
        return ids;
    }
}
