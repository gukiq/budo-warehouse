package org.budo.warehouse.web.launcher;

import budo.Launcher;

/**
 * @author limingwei
 */
public class Application {
    // 配置文件放在C:\.budo.config\.config.cache\.budo-warehouse-web-launcher.properties，C为当前磁盘，如果项目运行在D盘则为D
    // Linux环境则为 \.budo.config\.config.cache\.budo-warehouse-web-launcher.properties
    // 内容如下
    // 本地开发测试时存储配置信息的数据库
    // db.url=jdbc:mysql://127.0.0.1/budo_warehouse_100
    // db.username=root
    // db.password=
    // 暂固定为N/A
    // dubbo.registry.address=N/A
    public static void main(String[] args) {
        Launcher.main(args);
    }
}