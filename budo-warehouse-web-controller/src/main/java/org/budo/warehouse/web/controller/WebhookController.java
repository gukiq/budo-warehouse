package org.budo.warehouse.web.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.budo.warehouse.logic.api.DataConsumer;
import org.budo.warehouse.logic.api.DataMessage;
import org.budo.warehouse.logic.api.DataMessagePojo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

/**
 * @author lmw
 */
@Slf4j
@Controller
public class WebhookController {
    @Resource(name = "dispatcherDataConsumer")
    private DataConsumer dataConsumer;

    /**
     * https://gitee.com/help/articles/4186
     */
    @RequestMapping("webhook/gitee")
    public void gitee(HttpServletRequest request) {
        log.info("#27 gitee, request=" + request);
        DataMessage dataMessage = new DataMessagePojo();
        dataConsumer.consume(dataMessage);
    }

    @RequestMapping("webhook/aliyun")
    public void aliyun(HttpServletRequest request) {
        log.info("#35 aliyun, request=" + request);
        DataMessage dataMessage = new DataMessagePojo();
        dataConsumer.consume(dataMessage);
    }

    @RequestMapping("webhook/gitlib")
    public void gitlib(HttpServletRequest request) {
        log.info("#42 gitlib, request=" + request);
        DataMessage dataMessage = new DataMessagePojo();
        dataConsumer.consume(dataMessage);
    }
}