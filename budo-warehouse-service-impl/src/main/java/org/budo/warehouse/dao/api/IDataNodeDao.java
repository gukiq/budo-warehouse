package org.budo.warehouse.dao.api;

import java.util.List;

import org.budo.support.dao.page.Page;
import org.budo.warehouse.service.entity.DataNode;

/**
 * @author limingwei
 */
public interface IDataNodeDao {
    List<DataNode> listSourceDataNodes(Page page);

    DataNode findById(Integer id);

    List<DataNode> listMysqlNodes(Page page);
}