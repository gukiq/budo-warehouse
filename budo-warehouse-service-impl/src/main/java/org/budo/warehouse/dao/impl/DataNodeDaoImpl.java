package org.budo.warehouse.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.budo.mybatis.dao.MybatisDao;
import org.budo.support.dao.page.Page;
import org.budo.warehouse.dao.api.IDataNodeDao;
import org.budo.warehouse.service.entity.DataNode;
import org.springframework.stereotype.Repository;

/**
 * @author limingwei
 */
@Repository
public class DataNodeDaoImpl implements IDataNodeDao {
    @Resource
    private MybatisDao mybatisDao;

    @Override
    public List<DataNode> listSourceDataNodes(Page page) {
        String sql = " SELECT * FROM t_datanode WHERE id IN ( SELECT source_datanode_id FROM t_pipeline WHERE deleted_at IS NULL OR deleted_at='' ) ";
        return mybatisDao.listBySql(DataNode.class, sql, null, page);
    }

    @Override
    public List<DataNode> listMysqlNodes(Page page) {
        String sql = " SELECT * FROM t_datanode WHERE url LIKE 'jdbc:mysql://%' ";
        return mybatisDao.listBySql(DataNode.class, sql, null, page);
    }

    @Override
    public DataNode findById(Integer id) {
        return mybatisDao.findById(DataNode.class, id);
    }
}