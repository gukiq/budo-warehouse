package org.budo.warehouse.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.budo.ehcache.config.annotation.EhCacheConfig;
import org.budo.support.dao.page.Page;
import org.budo.warehouse.dao.api.IDataNodeDao;
import org.budo.warehouse.service.api.IDataNodeService;
import org.budo.warehouse.service.entity.DataNode;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * @author limingwei
 */
@Component
public class DataNodeServiceImpl implements IDataNodeService {
    @Resource
    private IDataNodeDao dataNodeDao;

    @Override
    public List<DataNode> listSourceDataNodes(Page page) {
        return dataNodeDao.listSourceDataNodes(page);
    }

    @Override
    public DataNode findById(Integer id) {
        return dataNodeDao.findById(id);
    }

    @EhCacheConfig(timeToLiveSeconds = 60, timeToIdleSeconds = 30, maxElementsInMemory = 100, maxElementsOnDisk = 200, graph = true)
    @Cacheable("DataNodeServiceFindByIdCached")
    @Override
    public DataNode findByIdCached(Integer id) {
        return dataNodeDao.findById(id);
    }

    @Override
    public List<DataNode> listMysqlNodes(Page page) {
        return dataNodeDao.listMysqlNodes(page);
    }
}